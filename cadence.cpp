/***************
 * Title: cadence.cpp
 * Author: Emma Ascolese
 * E-mail: eascoles@nd.edu
 *
 * finds words from a license plate and shows the shortest one 
 * ****************/

#include <iostream>
#include <string>
#include <vector>
#include <cctype>
#include <list>
#include <climits>
int SZ = 26;

/*****************
 * Function Name: tie 
 * Pre-conditions: int*, int*
 * Post- conditions: bool
 * compares two words to establish who won a tie
 * ***************/
bool tie(int* one, int* two){
	for (int i=0; i<SZ; i++){
		if (one[i]<two[i])
			return false;
	}
}

/*******************
 * Function name: count 
 * pre-conditions: string
 * post-conditions: int *
 * counts the number of each letter 
 * *****************/

int* count(std::string word){
	int array[26]={0};
	for (int i = 0; i<word.size(); i++){
		int val = tolower(word[i]) - 97;
		if (val>=0 && val < SZ)
			array[val]++;
	}
	return array; 
} 

/************
 * Function name: get shortest
 * pre-conditions: list<string>&, string 
 * post-conditions: list<string>
 * makes a list of the shortest words
 * ************/

std::list<std::string> getShortest(std::list<std::string>& dict, std::string plate){
	unsigned int size = UINT_MAX;
	std::list<std::string> shortest;
	for (std::string wd: dict){
		if(tie(count(wd), count(plate))){
			if(wd.size()<size){
				shortest.clear();
				shortest.push_back(wd);
				size=wd.size();
			}
		}
		else if(wd.size() == size) 
			shortest.push_back(wd);
	} 
return shortest;
}

/**************
 * Function name: main
 * pre-condition: int argc, char** argv
 * post-condition: int 
 *
 * main driver
 * **********/

int main(int argc, char ** argv){
	std::string plate= "EA104OS";
	std::list<std::string> dic;
	dic.push_back("Se");
	dic.push_back("EATS");
	dic.push_back("SAY");
	dic.push_back("NaY");
	dic.push_back("EASTER");
	dic.push_back("oats");
	dic.push_back("Essie");
	dic.push_back("Silly");
	dic.push_back("see");

	std::list<std::string> shortest = getShortest(dic, plate);
	
	std::cout<< "The shortest words are as folllows: " << std::endl;
	for (auto it = shortest.begin(); it != shortest.end(); it++){
		std::cout<<(*it)<<std::endl;
	}
return 0;
}
