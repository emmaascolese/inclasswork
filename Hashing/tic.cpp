/**********************
 * File Name: tic.cpp 
 * Author: Emma Ascolese
 * E-mail: eascoles@nd.edu
 * This file uses a hash table of all the possible tic tac toe games and who won them to determine who won a game of tic tac toe, utilizing a function to convert the board to an integer value. However, I was unable to successfully make the hashtable this would need, but it is an algorithm that would solve the problem
 * *****************/
#include <iostream> 

const int SIZE=3; //assuming the board is 3 by 3 

enum board { empty, x, o };

board winningNums[19683]; //3^9
/********************
 * Name: hasWon
 * PreConditions: int 
 * PostConditions: enum 
 * Looks in the hash table for who won the game associated with the int b, either no one (empty), x, or o
 * ******************/

board hasWon( int b) {
	return winningNums[b];
}

/**********************
 * Function Name: converttoInt
 * Preconditions: 2D array of enums
 * PostConditions: int 
 * converts the values of x's and o's to an integer value to be found in the arry
 * ********************/
int converttoInt(board b[SIZE][SIZE]){
	int sum = 0;
	for(int i = 0; i< SIZE; i++){
		for (int j = 0; j < SIZE; j++){
			int val = b[i][j];
			sum = sum * SIZE + val; 
		}
	}
	return sum; 
}

//now the last thing to do is look for the value returned in the hastable, that I was unable to create 

int main(){
	//call the functions and use the lookup table to see who has one 
	return 0;
}
